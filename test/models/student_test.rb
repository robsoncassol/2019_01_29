require 'test_helper'
 

class StudentTest < ActiveSupport::TestCase
    test "should save student" do
        student = Student.new
        student.cpf = "11122233300"
        student.grade = 1
        assert student.save
    end

    test "should not save student without CPF" do
        student = Student.new
        assert_not student.save
    end

    test "should not save student without grade" do
        student = Student.new
        student.cpf = "11122233300"
        assert_not student.save
    end

    test "should not accept student with grade less than 450" do
        student = Student.new
        student.cpf = "11122233300"
        student.grade = 451
        assert student.admitted?
    end
end