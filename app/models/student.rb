class Student < ApplicationRecord
  validates :cpf, presence: true, uniqueness: true
  validates :grade, presence: true, numericality: { only_integer: true}

  def admitted?
    return self.grade > 450
  end

end
